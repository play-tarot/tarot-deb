#!/bin/bash

apt-get update || exit 1

apt-get install -y \
	build-essential \
	dbus \
	dbus-x11 \
	libcairo2-dev \
	libglib2.0-dev \
	libgtk-3-0 \
	libgtk-3-dev \
	libreadline-dev \
	libxml2 \
	libxml2-dev \
	nettle-dev \
	pkg-config \
	valgrind \
	debhelper \
	dh-make \
	wget \
	reprepro \
    || exit 1

export $(dbus-launch)

wget https://play-tarot.frama.io/tarot-releases/tarot-latest.tar.gz || exit 1
tar xf tarot-latest.tar.gz || exit 1
VERSION=$(cat tarot-*/.version)
mv tarot-latest.tar.gz ./tarot_$VERSION.orig.tar.gz || exit 1
tar xf tarot_$VERSION.orig.tar.gz || exit 1
cd tarot-$VERSION || exit 1
cp -RT ../debian ./debian || exit 1
echo "$GPG_KEY" | gpg --import || exit 1
dpkg-buildpackage "-k$GPG_FINGERPRINT" || exit 1
mkdir -p ../public/conf || exit 1
(cat ../conf/distributions ; echo "SignWith: $GPG_FINGERPRINT") > ../public/conf/distributions || exit 1
cd ../public || exit 1
gpg --armor --output tarot.gpg --export "$GPG_FINGERPRINT" || exit 1

for deb in $PWD/../*.deb
do
    echo "Importing $deb..."
    reprepro includedeb stable $deb || exit 1
done

for dsc in ../*.dsc
do
    echo "Importing $dsc..."
    reprepro includedsc stable $dsc || exit 1
done

echo "Checking..."
reprepro check stable || exit 1

if test "x$CI_PAGES_URL" = "x"
then
    export CI_PAGES_URL="https://play-tarot.frama.io/tarot-deb"
fi

echo "deb $CI_PAGES_URL/ stable main" > tarot.list

<?xml version="1.0" encoding="UTF-8"?>
<!-- tarot implements the rules of the tarot game
Copyright (C) 2019  Vivien Kraus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">
  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template name="print">
    <xsl:param name="version" />
    <xsl:param name="author" />
    <xsl:param name="email" />
    <xsl:param name="deb-date" />
    <xsl:param name="summary" />
    <xsl:text>tarot (</xsl:text><xsl:value-of select="$version" /><xsl:text>-1) UNRELEASED; urgency=medium&#xA;&#xA;  * </xsl:text><xsl:value-of select="normalize-space($summary)" /><xsl:text>&#xA;&#xA; -- </xsl:text><xsl:value-of select="$author" /><xsl:text> &lt;</xsl:text><xsl:value-of select="$email" /><xsl:text>&gt;  </xsl:text><xsl:value-of select="$date" /><xsl:text>&#xA;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="releases">
    <xsl:if test="$snapshot = 'yes'">
      <xsl:call-template name="print">
	<xsl:with-param name="version" select="$version" />
	<xsl:with-param name="author" select="$author" />
	<xsl:with-param name="email" select="$email" />
	<xsl:with-param name="deb-date" select="$date" />
	<xsl:with-param name="summary">
	  Development snapshot
	</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="release">
    <xsl:call-template name="print">
      <xsl:with-param name="version" select="@version" />
      <xsl:with-param name="author" select="@author" />
      <xsl:with-param name="email" select="@email" />
      <xsl:with-param name="deb-date" select="@deb-date" />
      <xsl:with-param name="summary">
	<xsl:apply-templates select="summary" />
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="summary">
      <xsl:apply-templates />
  </xsl:template>
</xsl:stylesheet>
